import React, {Component} from 'react';
import './todolist.less';

class TodoList extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      data: [],
      number: 1
    };
    this.add = this.add.bind(this);
    console.log('constructor');
  }

  render() {
    console.log('render');
    const {status} = this.props;
    const {data} = this.state;
    return (
      <div style={{display: status === 'Show' ? 'none' : 'block'}}>
        <button onClick={this.add} className='add'>Add</button>
        <ul className='list-item'>
          {
            data.map((item, index) => <li key={index}><input type='text' placeholder={item} /></li>)
          }
        </ul>
      </div>
    );
  }

  add() {
    let {data, number} = this.state;
    data.push(`List Title${number++}`);
    this.setState({
      data: data,
      number: number
    })
  }

  componentDidMount() {
    console.log('componentDidMount');
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log('componentDidUpdate');
  }

  componentWillUnmount() {
    console.log('componentWillUnmount');
  }
}

export default TodoList;

