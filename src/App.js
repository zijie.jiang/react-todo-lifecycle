import React from 'react';
import './App.less';
import TodoList from "./components/TodoList";

class App extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            status: 'Show'
        };
        this.changeButtonStatus = this.changeButtonStatus.bind(this);
    }

    render() {
        const {status} = this.state;
        return (
            <div className='App'>
                <button onClick={this.changeButtonStatus}>{status}</button>
                <button onClick={() => location.reload()}>Refresh</button>
                <TodoList status={status}/>
            </div>
        );
    }

    changeButtonStatus() {
        let {status} = this.state;
        this.setState({status: status === 'Show' ? 'Hide' : 'Show'})
    }
}

export default App;
